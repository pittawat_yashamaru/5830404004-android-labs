package pittawat.phongboribun.kku.ac.th.lab6_5;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    private List<Address> list3;
    private RadioGroup rdGroup;
    private RadioButton useLatLng, useAddrs;
    private Button fetchBtn;
    private EditText editLat, editLng, editAddrs;
    private Integer selectId;
    private static final int LOCATION_REQUEST_CODE = 101;
    private String TAG;
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager,locationManager;
    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        selectId =0;
        useLatLng = (RadioButton) findViewById(R.id.useLatLng);
        useAddrs = (RadioButton) findViewById(R.id.useAddrs);
        editLat = (EditText) findViewById(R.id.inputLat);
        editLng = (EditText) findViewById(R.id.inputLng);
        editAddrs = (EditText) findViewById(R.id.inputAddrs);
        fetchBtn = (Button) findViewById(R.id.fetchBtn);
        rdGroup = (RadioGroup) findViewById(R.id.rdGroup);
        fetchBtn.setOnClickListener(this);
        rdGroup.setOnCheckedChangeListener(rgListener);
        editAddrs.setEnabled(false);

        checkLocation(); //check whether location service is enable or not in your  phone
        if (Build.VERSION.SDK_INT > 22) {

            //check run time permssion
            if (ActivityCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                //PERMISSION IS NOT GRANTED
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);

                return;
            } else {
                buildGoogleApiClient();
            }

        } else {
            buildGoogleApiClient();
        }

        TAG = this.getLocalClassName();
    }

    @Override
    public void onClick(View v) {
        try {
            if (selectId == 0) {
                Location loc = new Location("");
                loc.setLatitude(Double.parseDouble(editLat.getText().toString()));
                loc.setLongitude(Double.parseDouble(editLng.getText().toString()));
                getAddressFromLocation(loc, this, new GeocoderHandler());
            } else {
                String locationName = editAddrs.getText().toString();
                getAddressFromAddress(locationName, this, new GeocoderHandler());
            }


        } catch (Exception e) {
            Log.e(TAG, "I don't know",e);
        }

    }

    private final class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String dataAddr, dataLat, dataLng;
            Bundle bundle,bundle2;
            switch (message.what) {
                case 1:
                    bundle = message.getData();
                    dataAddr = bundle.getString("addressLatLng");
                    String[] splitStr = dataAddr.split("\\s+");
                    dataLat = splitStr[0];
                    dataLng = splitStr[1];
                    editLat.setEnabled(true);
                    editLng.setEnabled(true);
                    editLat.setText(dataLat);
                    editLng.setText(dataLng);

                    break;
                case 2:
                    bundle = message.getData();
                    dataAddr = bundle.getString("addressFromLoc");
                    editAddrs.setEnabled(true);
                    editAddrs.setText(dataAddr);
                    break;
                default:
                    dataAddr = null;
                    dataLat = null;
                    dataLng = null;
            }
            // replace by what you need to do
            //resultText.setText(result);

        }
    }

    private void setEnable(int id,final boolean active)
    {
        switch (id)
        {
            case R.id.useLatLng:
                editLat.setEnabled(active);
                editLng.setEnabled(active);
                editAddrs.setEnabled(!active);
                if (active)
                {
                    editLat.requestFocus();
                }
                break;
            case R.id.useAddrs:
                editAddrs.setEnabled(active);
                editLat.setEnabled(!active);
                editLng.setEnabled(!active);
                if (active)
                {
                    editAddrs.requestFocus();
                }
                break;
        }
    }

    RadioGroup.OnCheckedChangeListener rgListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int id) {
            switch (id)
            {
                case R.id.useLatLng:
                    setEnable(R.id.useLatLng,true);
                    selectId = 0;
                    break;

                case R.id.useAddrs:
                    setEnable(R.id.useAddrs,true);
                    selectId = 1;
                    break;
            }
        }
    };

    protected void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mLocationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            return;
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation != null) {
            editLat.setText(String.valueOf(mLocation.getLatitude()));
            editLng.setText(String.valueOf(mLocation.getLongitude()));
            Location loc = new Location("");
            loc.setLatitude(Double.parseDouble(editLat.getText().toString()));
            loc.setLongitude(Double.parseDouble(editLng.getText().toString()));
            getAddressFromLocation(loc, this, new GeocoderHandler());
        } else {
            Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details. FusedLocationApi
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    @Override
    public void onLocationChanged(Location location) {

        // You can now create a LatLng Object for use with maps
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
    }

    private boolean checkLocation() {
        if(!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " + "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    public void getAddressFromLocation(final Location location, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                try {
                    list3 = geocoder.getFromLocation(
                            location.getLatitude(), location.getLongitude(), 1);
                    if (list3 != null && list3.size() > 0) {
                        Address address = list3.get(0);
                        result = address.getAddressLine(0) + ", "
                                + address.getLocality();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Impossible to connect to Geocoder", e);
                } finally {
                    Message msg = Message.obtain();
                    msg.setTarget(handler);
                    if (result != null) {
                        msg.what = 2;
                        Bundle bundle = new Bundle();
                        bundle.putString("addressFromLoc", result);
                        msg.setData(bundle);
                    } else
                        msg.what = 0;
                    msg.sendToTarget();
                }
            }
        };
        thread.start();
    }

    public void getAddressFromAddress(final String address, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String resultLatLng = null;
                try {
                    List<Address> list2 = geocoder.getFromLocationName(address, 1);
                    if (list2 != null && list2.size() > 0) {
                        Address address = list2.get(0);
                        resultLatLng = address.getLatitude() + " " + address.getLongitude();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Impossible to connect to Geocoder", e);
                } finally {
                    Message msg = Message.obtain();
                    msg.setTarget(handler);
                    if (resultLatLng != null) {
                        msg.what = 1;
                        Bundle bundle = new Bundle();
                        bundle.putString("addressLatLng", resultLatLng);
                        msg.setData(bundle);
                    } else
                        msg.what = 0;
                    msg.sendToTarget();
                }
            }
        };
        thread.start();
    }


}