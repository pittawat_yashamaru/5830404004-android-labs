package pittawat.phongboribun.kku.ac.th.lab6_3;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup rdGroup;
    private RadioButton useLatLng, useAddrs;
    private Button fetchBtn;
    private EditText editLat, editLng, editAddrs;
    private TextView resultText;
    private String TAG;
    private Integer selectId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fetchBtn = (Button) findViewById(R.id.fetchBtn);
        fetchBtn.setOnClickListener(this);
        rdGroup = (RadioGroup) findViewById(R.id.rdGroup);
        rdGroup.setOnCheckedChangeListener(rgListener);
        useLatLng = (RadioButton) findViewById(R.id.useLatLng);
        useAddrs = (RadioButton) findViewById(R.id.useAddrs);
        editLat = (EditText) findViewById(R.id.inputLat);
        editLng = (EditText) findViewById(R.id.inputLng);
        editAddrs = (EditText) findViewById(R.id.inputAddrs);
        editAddrs.setEnabled(false);
        resultText = (TextView) findViewById(R.id.result);

        TAG = this.getLocalClassName();

    }


    RadioGroup.OnCheckedChangeListener rgListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int id) {
            switch (id)
            {
                case R.id.useLatLng:
                    setEnable(R.id.useLatLng,true);
                    selectId = 0;
                    break;

                case R.id.useAddrs:
                    setEnable(R.id.useAddrs,true);
                    selectId = 1;
                    break;
            }
        }
    };

    private void setEnable(int id,final boolean active)
    {
        switch (id)
        {
            case R.id.useLatLng:
                editLat.setEnabled(active);
                editLng.setEnabled(active);
                editAddrs.setEnabled(!active);
                if (active)
                {
                    editLat.requestFocus();
                }
                break;
            case R.id.useAddrs:
                editAddrs.setEnabled(active);
                editLat.setEnabled(!active);
                editLng.setEnabled(!active);
                if (active)
                {
                    editAddrs.requestFocus();
                }
                break;
        }
    }

    public void getAddressFromLocation(final Location location, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                try {
                    List<Address> list = geocoder.getFromLocation(
                            location.getLatitude(), location.getLongitude(), 1);
                    if (list != null && list.size() > 0) {
                        Address address = list.get(0);
                        result = address.getAddressLine(0) + ", "
                                + address.getLocality();
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Impossible to connect to Geocoder", e);
                } finally {
                    Message msg = Message.obtain();
                    msg.setTarget(handler);
                    if (result != null) {
                        msg.what = 2;
                        Bundle bundle = new Bundle();
                        bundle.putString("addressFromLoc", result);
                        msg.setData(bundle);
                    } else
                        msg.what = 0;
                    msg.sendToTarget();
                }
            }
        };
        thread.start();
    }

    public void getAddressFromAddress(final String address, final Context context, final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                String result = null;
                    try {
                        List<Address> list2 = geocoder.getFromLocationName(address, 1);
                        if (list2 != null && list2.size() > 0) {
                            Address address = list2.get(0);
                            result = "Lat : " + address.getLatitude() + ", " + "Lng : " + address.getLongitude();
                        }
                    } catch (IOException e) {
                        Log.e(TAG, "Impossible to connect to Geocoder", e);
                    } finally {
                        Message msg = Message.obtain();
                        msg.setTarget(handler);
                        if (result != null) {
                            msg.what = 1;
                            Bundle bundle = new Bundle();
                            bundle.putString("addressFromName", result);
                            msg.setData(bundle);
                        } else
                            msg.what = 0;
                        msg.sendToTarget();
                    }
                }
            };
        thread.start();
    }


    @Override
    public void onClick(View v) {
        try {
                if (selectId == 0) {
                    Location loc = new Location("");
                    loc.setLatitude(Double.parseDouble(editLat.getText().toString()));
                    loc.setLongitude(Double.parseDouble(editLng.getText().toString()));
                    getAddressFromLocation(loc, this, new GeocoderHandler());
                } else {
                    String locationName = editAddrs.getText().toString();
                    getAddressFromAddress(locationName, this, new GeocoderHandler());
                }


        } catch (Exception e) {
            Log.e(TAG, "I don't know",e);
        }

    }

    private final class GeocoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String result;
            Bundle bundle;
            switch (message.what) {
                case 1:
                    bundle = message.getData();
                    result = bundle.getString("addressFromName");
                    break;
                case 2:
                    bundle = message.getData();
                    result = bundle.getString("addressFromLoc");
                    break;
                default:
                    result = null;
            }
            // replace by what you need to do
            resultText.setText(result);
        }
    }
}
