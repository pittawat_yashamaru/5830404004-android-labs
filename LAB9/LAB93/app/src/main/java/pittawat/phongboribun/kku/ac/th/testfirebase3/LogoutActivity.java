package pittawat.phongboribun.kku.ac.th.testfirebase3;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class LogoutActivity extends AppCompatActivity {

    private FirebaseDatabase database;
    private String TAG = "123456";
    private DatabaseReference myRef;
    private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        tv = (TextView) findViewById(R.id.tv);

        ((Button) findViewById(R.id.logoutBtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                onBackPressed();
            }
        });

        String email = getIntent().getStringExtra("email");
        tv.setText(email + " is logged in successfully");

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("messgase");
        writeNewUser("pittawat","phonbongribun","pittawat@kkumail.com");
        readNewUser();
    }


    private void writeNewUser(String userId, String name, String email) {
        String key = myRef.child("users").push().getKey();
        User user = new User(name,email);
        Map<String, Object> userValues = user.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/users/" + key, userValues);
        myRef.updateChildren(childUpdates);
    }

    private void readNewUser() {
        myRef.child("users").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class);
                Toast.makeText(LogoutActivity.this,"Username is " + user.toMap().get("username"),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class);
                Toast.makeText(LogoutActivity.this,"Email is " + user.toMap().get("email") + user.toMap().get("username"),Toast.LENGTH_LONG).show();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
