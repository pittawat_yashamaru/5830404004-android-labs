package pittawat.phongboribun.kku.ac.th.webapicaller;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    private String addrsVal, lat="", lng="";
    private static String apiURL = "https://maps.googleapis.com/maps/api/geocode/json?address=";
    private static String apiKey = "AIzaSyC8tCX3bJl2PgjnxUafBeGzxuYZbGAWusY";
    private EditText addrsText;
    private TextView resultView;
    private Button queryButton;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        addrsText = (EditText) findViewById(R.id.addrsText);
        queryButton = (Button) findViewById(R.id.queryButton);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        resultView = (TextView) findViewById(R.id.resultView);

        queryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new RetrieveFeedTask().execute();
            }
        });

    }


    class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            resultView.setText("");
        }

        protected String doInBackground(Void... urls) {
            addrsVal = addrsText.getText().toString();
            // Do some validation here

            try {
                Log.i("INFO", "url = " + addrsVal);
                URL urlLocation = new URL(apiURL + addrsVal + "&key=" + apiKey);
                HttpURLConnection urlConnection = (HttpURLConnection) urlLocation.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                }
                finally{
                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {

            if(response == null) {
                response = "THERE WAS AN ERROR";
            }
            progressBar.setVisibility(View.GONE);

            try {
                JSONObject object = new JSONObject(response);
                JSONArray arr = object.getJSONArray("results");
                JSONObject collect = arr.getJSONObject(0).getJSONObject("geometry").getJSONObject("location");
                Log.i("INFO", String.valueOf(collect));
                    lat = collect.getString("lat");
                    lng = collect.getString("lng");
                resultView.setText("Latitude of " + addrsVal + " = " + lat + "\n" + "Longitude of " + addrsVal + " = " + lng);
            }   catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
