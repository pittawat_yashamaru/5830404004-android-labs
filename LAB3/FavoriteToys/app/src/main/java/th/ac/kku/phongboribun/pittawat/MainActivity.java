package th.ac.kku.phongboribun.pittawat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView setGreen = (TextView)findViewById(R.id.green);
        setGreen.setText("Green");
        setGreen.setGravity(Gravity.CENTER);
        setGreen.setTextSize(30);
    }
}
