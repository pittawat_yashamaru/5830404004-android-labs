package th.ac.kku.phongboribun.pittawat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button) findViewById(R.id.btnSubmit);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText inputName = (EditText) findViewById(R.id.editName);
                EditText inputNumber = (EditText) findViewById(R.id.editPhone);
                String name = getString(R.string.defName);
                String phone = getString(R.string.defPhone);
                String message = getString(R.string.message);
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                intent.putExtra("name", name);
                intent.putExtra("phone", phone);
                intent.putExtra("wordResult", message);
                startActivity(intent);
            }
        });
    }
}
