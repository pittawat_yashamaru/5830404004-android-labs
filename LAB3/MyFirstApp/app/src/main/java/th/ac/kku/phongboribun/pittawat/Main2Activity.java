package th.ac.kku.phongboribun.pittawat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        TextView textView = (TextView)findViewById(R.id.boxMessage);
        Bundle bundle = getIntent().getExtras();
        String getName = bundle.getString("name");
        String getPhone = bundle.getString("phone");
        String getWord = bundle.getString("wordResult");
        textView.setText(getName + " " + getWord + " " + getPhone);
    }
}
