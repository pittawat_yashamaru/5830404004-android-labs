package pittawat.phongboribun.kku.ac.th.mycalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private RadioGroup rgOperator;
    private Button btnCalculate;
    private EditText val1,val2;
    private TextView tvResult,desSw;
    private Switch btnSwitch;
    private float num1 = 0,num2 = 0,result = 0;
    private int chkToast=1;
    private boolean swState = false ,accept = false;
    private long startTime ,endTime, compuTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        val1 = (EditText) findViewById(R.id.op1);
        val2 = (EditText) findViewById(R.id.op2);
        btnCalculate = (Button) findViewById(R.id.btnCal);
        tvResult = (TextView) findViewById(R.id.resultView);
        rgOperator = (RadioGroup) findViewById(R.id.btnRadio);
        btnSwitch = (Switch) findViewById(R.id.btnSwitch);
        desSw = (TextView) findViewById(R.id.desSw);
        btnCalculate.setOnClickListener(this);
        btnSwitch.setOnClickListener(this);
        rgOperator.setOnCheckedChangeListener(rgListener);

    }

    RadioGroup.OnCheckedChangeListener rgListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            if (i == R.id.divide) {
                calculate(i);
            }
        }
    };

        public void onClick(View view) {
            if (view == btnCalculate) {
                calculate(rgOperator.getCheckedRadioButtonId());
            }
            if (view == btnSwitch) {
                swState = btnSwitch.isChecked();
                if (swState) {
                    desSw.setText("ON");
                } else {
                    desSw.setText("OFF");
                }
            }
        }

        private void calculate(int id) {
            acceptNumbers();
            if (accept) {
                switch (id) {
                    case R.id.add:
                        result = num1 + num2;
                        break;
                    case R.id.subtract:
                        result = num1 - num2;
                        break;
                    case R.id.multiply:
                        result = num1 * num2;
                        break;
                    case R.id.divide:
                        if (val2.getText().toString().equals("0") && (rgOperator.getCheckedRadioButtonId() == R.id.divide)) {
                            chkToast = 2;
                            showToast();
                            accept = false;
                        } else result = num1 / num2;
                        break;
                }
                endTime = System.currentTimeMillis() / 1000;
                compuTime = endTime - startTime;
                tvResult.setText(" = " + result);
                Log.d("Calculation  ", "computation time = " + compuTime);
            }
        }

        private void acceptNumbers() {
            startTime = System.currentTimeMillis() / 1000;
            if (!val1.getText().toString().equals("") && !val2.getText().toString().equals("")) {
                try {
                    num1 = Float.parseFloat(val1.getText().toString());
                    num2 = Float.parseFloat(val2.getText().toString());
                    accept = true;
                } catch (NumberFormatException e) {
                    accept = false;
                    chkToast = 3;
                    showToast();
                }
            } else if (val1.getText().toString().equals("") || val2.getText().toString().equals("")) {
                chkToast = 1;
                showToast();
                accept = false;
            } else {
                accept = true;
            }
        }

        private void showToast() {
            if (chkToast == 3) {
                Toast.makeText(MainActivity.this, "Please enter only the number", Toast.LENGTH_SHORT).show();
            } else if (chkToast == 2) {
                Toast.makeText(MainActivity.this, "Please divided by a non-zero number", Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(MainActivity.this, "Please enter the value", Toast.LENGTH_SHORT).show();
        }
    }